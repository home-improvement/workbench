// leg.scad
// Designed by Kevin Cole <ubuntourst@hacdc.org> 2018.12.05 (kjc)
//
// A cylindrical leg for the workbench.
//
// NOTE: Formerly square, as in wood, defined as:
//
//              cube([X, Y, Z]);
//
//       Now round, as in PVC pipes or metal tubing? defined as:
//
//              cylinder(H, R, resolution);
//

module Leg(length, diameter) {
  color("White")
    cylinder(h=length, d=diameter, $fn=200);
}

// Demo

Table_X = 48.00;     // Width of "nook"
Table_Y = 32.00;     // Small enough to fit through a door
Table_Z =  1.50;

// Legs are evolving to PVC piping. See additional notes.
//
Leg_X =  1.90;
Leg_Y =  1.90;
Leg_D =  1.90;       // leg diameter
Leg_R =  Leg_D / 2;  // leg radius
Leg_Z = 33;

union() {
  translate([Leg_R, Leg_R, 0])
    Leg(Leg_Z, Leg_X);  // Front Left
  translate([Leg_R, Leg_R + (Table_Y - Leg_Y), 0])
    Leg(Leg_Z, Leg_X);  // Rear Left
  translate([Leg_R + (Table_X - Leg_X), Leg_R, 0])
    Leg(Leg_Z, Leg_X);  // Front Right
  translate([Leg_R + (Table_X - Leg_X), Leg_R + (Table_Y - Leg_Y), 0])
    Leg(Leg_Z, Leg_X);  // Rear Right
}
