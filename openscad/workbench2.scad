// Workbench
// Designed by Kevin Cole <ubuntourst@hacdc.org> 2018.12.05 (kjc)
//
// A work in regress. Also attempting to duplicate in POV-Ray,
// and, eventually(?) Blender, mostly as practical excuses to
// learn both of those systems better.
//
// Not shown here, but dreamed of: Wheels, foldout (fold-down?)
// 45-degree drafting surface w/ monitor cum lightbox, shelving,
// drawers (possibly as roll-away cabinet).
//
//         *** WARNING! DANGER, WILL ROBINSON! ***
//
// Due to the relative complexity DO NOT try to do a full
// render or export to STL !!! It will take bloody forever
// (and may not even finish before blowing up). Also, the
// full render loses all the color information so lovingly
// added.
//
// Actually, from the command line, if you wait 30 minutes,
//
//     openscad -o workbench.stl workbench.scad
//
// will create the STL file.
//

use <tabletop.scad>
use <leg.scad>
use <pegboard.scad>

// mm = 25.4;  // millimeters per inch (unused)

Room_Z = 7.5 * 12;   // Approximate room height in inches

Table_X = 48.00;     // Width of "nook"
Table_Y = 32.00;     // Small enough to fit through a door
Table_Z =  1.50;

Pegboard_X = 48.00;  // Width of the table
Pegboard_Y =  0.25;  // How thick are pegboards, typically?
Pegboard_Z = 36.00;  // Unused. Use room height to compute.

// Legs are evolving to PVC piping. See additional notes.
//
Leg_X =  1.90;
Leg_Y =  1.90;
Leg_D =  1.90;       // leg diameter
Leg_R =  Leg_D / 2;  // leg radius
Leg_Z = 33;

union() {
  translate([Leg_R, Leg_R, 0])
    Leg(Leg_Z, Leg_X);  // Front Left
  translate([Leg_R, Leg_R + (Table_Y - Leg_Y), 0])
    Leg(Leg_Z, Leg_X);  // Rear Left
  translate([Leg_R + (Table_X - Leg_X), Leg_R, 0])
    Leg(Leg_Z, Leg_X);  // Front Right
  translate([Leg_R + (Table_X - Leg_X), Leg_R + (Table_Y - Leg_Y), 0])
    Leg(Leg_Z, Leg_X);  // Rear Right
  translate([0, 0, Leg_Z])
    TableTop(Table_X, Table_Y, Table_Z);
  translate([0, Table_Y - Pegboard_Y, Leg_Z + Table_Z])
    Pegboard(Pegboard_X, Pegboard_Y, Room_Z - (Leg_Z + Table_Z));
}
