// pegboard.scad
// Designed by Kevin Cole <ubuntourst@hacdc.org> 2018.12.05 (kjc)
//
// A pegboard for the workbench.
//
// NOTE: The values 3 and 6 below (in the translate and cylinder) are
//       for "padding" to compensate for OpenSCAD's "edge cases".
//       When differencing two shapes, the shape to be subtracted
//       should be larger than necessary in order to be sure that
//       OpenSCAD does not become confused about the boundary between
//       the edges of the shape that remains and the shape that is
//       being removed.
//
// NOTE: Translate FIRST then rotate! Otherwise Z values become Y and
//       everything becomes more confusing than it needs to be.
//

module Pegboard(x, y, z) {
  color("BurlyWood")
  difference() {
    cube([x, y, z]);
    for(across=[1 : 2 : x - 1])
      for(up=[1 : 2 : z - 1])
        translate([across, 3, up])
          rotate(a=[90, 0, 0])
            cylinder(h=6, d=0.5, $fn=200);
  }
}

// Demo

Room_Z = 7.5 * 12;   // Approximate room height in inches

Table_X = 48.00;     // Width of "nook"
Table_Y = 32.00;     // Small enough to fit through a door
Table_Z =  1.50;

Pegboard_X = 48.00;  // Width of the table
Pegboard_Y =  0.25;  // How thick are pegboards, typically?
Pegboard_Z = 36.00;  // Unused. Use room height to compute.

// Legs are evolving to PVC piping. See additional notes.
//
Leg_X =  1.90;
Leg_Y =  1.90;
Leg_D =  1.90;       // leg diameter
Leg_R =  Leg_D / 2;  // leg radius
Leg_Z = 33;

translate([0, Table_Y - Pegboard_Y, Leg_Z + Table_Z])
  Pegboard(Pegboard_X, Pegboard_Y, Room_Z - (Leg_Z + Table_Z));
