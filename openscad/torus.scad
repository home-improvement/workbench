//  Copied by Kevin Cole <ubuntourist@hacdc.org> 2018.12.24 (kjc)
//
//  An OpenSCAD implementation of POV-Ray's torus shape. To use, add the
//  following lines to your main object file:
//
//    use <torus.scad>
//       ...
//    torus(major, minor)
//
//      * major = radius from "origin" to inside center of tube
//      * Minor = radius from inside center of tube to rim of tube
//
//  The default position of the torus is centered (embedded) around
//  the origin, and laying flat like a donut on a table. Add minor
//  radius value to Z to lift the torus fully above the orign plane.
//
//  See https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/2D_to_3D_Extrusion
//
//  This being a common, generic, very useful shape, it should probably live
//  in ~/.local/share/OpenSCAD/libraries/ (or wherever your OS keeps such.
//  See  https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries)
//

module torus(major, minor) {
  rotate_extrude(convexity=10, $fn=200)
    translate([major, 0, 0])
      circle(r=minor);
}
