// Workbench
// Designed by Kevin Cole <ubuntourst@hacdc.org> 2018.12.05 (kjc)
//
// A work in regress. Also attempting to duplicate in POV-Ray,
// and, eventually(?) Blender, mostly as practical excuses to
// learn both of those systems better.
//
// Not shown here, but dreamed of: Wheels, foldout (fold-down?)
// 45-degree drafting surface w/ monitor cum lightbox, shelving,
// drawers (possibly as roll-away cabinet).
//
//         *** WARNING! DANGER, WILL ROBINSON! ***
//
// Due to the relative complexity DO NOT try to do a full
// render or export to STL !!! It will take bloody forever
// (and may not even finish before blowing up). Also, the
// full render loses all the color information so lovingly
// added.
//
// Actually, from the command line, if you wait 30 minutes,
//
//     openscad -o workbench.stl workbench.scad
//
// will create the STL file.
//

// mm = 25.4;  // millimeters per inch (unused)

Room_Z = 7.5 * 12;   // Approximate room height in inches

Table_X = 48.00;     // Width of "nook"
Table_Y = 32.00;     // Small enough to fit through a door
Table_Z =  1.50;

Pegboard_X = 48.00;  // Width of the table
Pegboard_Y =  0.25;  // How thick are pegboards, typically?
Pegboard_Z = 36.00;  // Unused. Use room height to compute.

// Legs are evolving to PVC piping. See additional notes.
//
Leg_X =  1.90;
Leg_Y =  1.90;
Leg_D =  1.90;       // leg diameter
Leg_R =  Leg_D / 2;  // leg radius
Leg_Z = 33;

/////////////////////////////////////////////////////////////////////////
// Table top                                                           //
/////////////////////////////////////////////////////////////////////////

module TableTop(x, y, z) {
  color("LemonChiffon")
    cube([x, y, z]);
}

/////////////////////////////////////////////////////////////////////////
// Legs                                                                //
//                                                                     //
// NOTE: Formerly square, as in wood, defined as:                      //
//          cube([X, Y, Z]);                                           //
//       Now round, as in PVC pipes or metal tubing? defined as:       //
//          cylinder(H, R, resolution);                                //
/////////////////////////////////////////////////////////////////////////

module Leg(length, diameter) {
  color("White")
    cylinder(h=length, d=diameter, $fn=200);
}

/////////////////////////////////////////////////////////////////////////
// Pegboard                                                            //
//                                                                     //
// NOTE: The 3 and 6 values below (in the translate and cylinder)      //
// are for "padding" to compensate for OpenSCAD's "edge cases".        //
// When differencing two shapes, the shape to be subtracted should     //
// be larger than necessary in order to be sure that OpenSCAD does     //
// not become confused about the boundary between the edges of the     //
// shape that remains and the shape that is being removed.             //
//                                                                     //
// NOTE: Translate FIRST then rotate! Otherwise Z values become Y and  //
// everything becomes more confusing than it needs to be.              //
/////////////////////////////////////////////////////////////////////////

module Pegboard(x, y, z) {
  color("BurlyWood")
  difference() {
    cube([x, y, z]);
    for(across=[1 : 2 : x - 1])
      for(up=[1 : 2 : z - 1])
        translate([across, 3, up])
          rotate(a=[90, 0, 0])
            cylinder(h=6, d=0.5, $fn=200);
  }
}

union() {
  translate([Leg_R, Leg_R, 0])
    Leg(Leg_Z, Leg_X);  // Front Left
  translate([Leg_R, Leg_R + (Table_Y - Leg_Y), 0])
    Leg(Leg_Z, Leg_X);  // Rear Left
  translate([Leg_R + (Table_X - Leg_X), Leg_R, 0])
    Leg(Leg_Z, Leg_X);  // Front Right
  translate([Leg_R + (Table_X - Leg_X), Leg_R + (Table_Y - Leg_Y), 0])
    Leg(Leg_Z, Leg_X);  // Rear Right
  translate([0, 0, Leg_Z])
    TableTop(Table_X, Table_Y, Table_Z);
  translate([0, Table_Y - Pegboard_Y, Leg_Z + Table_Z])
    Pegboard(Pegboard_X, Pegboard_Y, Room_Z - (Leg_Z + Table_Z));
}
