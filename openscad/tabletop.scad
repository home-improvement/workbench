// tabletop.scad
// Designed by Kevin Cole <ubuntourst@hacdc.org> 2018.12.05 (kjc)
//
// The table top for the workbench.
//

module TableTop(x, y, z) {
  color("LemonChiffon")
    cube([x, y, z]);
}

// Demo

Table_X = 48.00;     // Width of "nook"
Table_Y = 32.00;     // Small enough to fit through a door
Table_Z =  1.50;

// Legs are evolving to PVC piping. See additional notes.
//
Leg_X =  1.90;
Leg_Y =  1.90;
Leg_D =  1.90;       // leg diameter
Leg_R =  Leg_D / 2;  // leg radius
Leg_Z = 33;

translate([0, 0, Leg_Z])
  TableTop(Table_X, Table_Y, Table_Z);
