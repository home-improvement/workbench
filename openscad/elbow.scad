//  elbow.scad
//
//  Written by Kevin Cole <ubuntourist@hacdc.org> 2018.12.24 (kjc)
//
//             DO NOT RENDER (OR EXPORT) !!!
//
//  It will take a bloody long time, and I don't know if it will
//  actually finish, or set your computer on fire. Also, a full
//  render or export removes any color information provided.
//

use <torus.scad>

Min_Wall =  0.145;                  // Minimum wall thickness
Avg_OD   =  1.90 + (Min_Wall * 2);  // Average Outer Diameter
Avg_OR   =  Avg_OD / 2;             // Average Outer Radius
Major    =  1.90;                   // tube diameter
Major    =  Avg_OD;                 // tube diameter
Minor    =  Major / 2;              // tube radius
H        = 2.0 + (5 / 16);          // See Charlotte Pipe, Inc. specs
G        = 1.0;                     // See Charlotte Pipe, Inc. specs

Tube_X =  1.90;
Tube_Y = 32.00;
Tube_Z =  1.90;
Tube_L = 3.00;            // tube length
Tube_D =  1.90;           // tube diameter
Tube_R =  Tube_D / 2;     // tube radius

module Elbow() {
  color("White")
  union() {
    intersection() {
      difference() {
        torus(Major, Avg_OR);             // Solid  outside
        torus(Major, Avg_OR - Min_Wall);  // Hollow inside
      }
      // "Rear left quarter"
      translate([-5, 0, -2.5])
        cube([5, 5, 5]);
    }
    // Extender
    translate([-1.0 * Major,0, 0.01])
      rotate([90, 0, 0])
        difference() {
          cylinder(h=(H - G) + 0.01, r=Avg_OR, $fn=200);
          translate([0, 0, -2])    // Hollow inside
            cylinder(h=(H - G) + 5, r=Avg_OR - Min_Wall, $fn=200);
        }
    // Extender
    translate([-0.01, 1.0 * Major, 0])
      rotate([0, 90, 0])
        difference() {
          cylinder(h=(H - G) + 0.01, r=Avg_OR, $fn=200);
          translate([0, 0, -2])    // Hollow inside
            cylinder(h=(H - G) + 5, r=Avg_OR - Min_Wall, $fn=200);
        }
  }
}

module Tube() {
  color("Red")
  difference() {
      cylinder(h=Tube_L, r=Tube_R, $fn=200);
      translate([0, 0, -10])
        cylinder(h=50, r=Tube_R - Min_Wall, $fn=200);
  }
}

union() {
  translate([0, 0, Minor])
    Elbow();
  translate([-1.0 * Major, 0, Tube_R + Min_Wall])
    rotate([90, 0, 0])
      Tube();
  translate([0, 1.0 * Major, Tube_R + Min_Wall])
    rotate([0, 90, 0])
      Tube();
}
