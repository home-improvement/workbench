# Workbench

Copyright &copy; 2018 Kevin Cole (ubuntourist@hacdc.org) 2018.12.25 (GPL)

This is an attempt to become more savvy with
[OpenSCAD](http://www.openscad.org/),
[POV-Ray](http://www.povray.org/), and maybe eventually
[Blender](https://www.blender.org/) while also planning out a fantasy
workbench.

## "The pipes, the pipes are calling"

See ACE Hardware wish list of Charlotte Pipe Company PVC pipes.

        1 1/2" refers to the minimum? inner diameter, apparently,  and
               is referred to as the "Nominal Size"
        1.90"  is the average Outside Diameter (OD)
        0.145" is the "Minimum Wall Thickness"
        H - G = how far a pipe can be shoved into a joint?

Using the values above, the inner diameter is more like 1.61".

Since this is an Xmas project, the wish list of ACE hardware being
modeled is saved as `sugar-plumbing-fairy.pdf`. Heh.

## Software tips

* In **OpneSCAD** I've had better luck with `translate` first, then
  `rotate`. I never seem to get coordinates in the correct orientation
  if I do it the other way around.

* The official POV-Ray tutorial points out that `torus` is a highly
  useful primative. Thus, it would behoove *OpenSCADians* to place the
  included `torus.scad` in a common library path. On Linux systems,
  the userland path is `~/.local/share/OpenSCAD/libraries/`. For other
  systems see the **Libraries** section of the [OpenSCAD User
  Manual](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries)

* There is both a monolitic (`workbench1`) and a modularized
  (`workbench2`) version of the workbench in both systems. They
  produce the same output, but it seemed there might be future
  advantages to decoupling all the pieces... Hence the evolution of
  `workbench1` into `workbench2`.

* According to [Wikipedia: Perforated
  hardboard](https://en.wikipedia.org/wiki/Perforated_hardboard) the
  hole size and spacing in my pegboard is incorrect. The holes should
  be smaller and closer together (1/4" and 1" respectively, as opposed
  to my 1/2" and 2"). However, for sake of aesthetics and rendering
  capabilities, my dimensions look better.

* `FreeCAD` can open *simple* `OpenSCAD` files. (`elbow.scad`
  opened. `workbench.scad` just sat and spun.)
