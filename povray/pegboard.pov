// pegboard.pov
//
// Pegboard for the workbench
//
// Copyright (c) 2018 Kevin Cole <ubuntourist@hacdc.org> 2018.12.12
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free
// Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.
//

#declare Pegboard = difference {
  box {
      <     0,            0,                      0      >,
      <Pegboard_X, Room_Y - (Leg_Y + Table_Y), Pegboard_Z>
      texture { T_Wood4 }  // T_Wood12 T_Wood22
    }
  #for (Grid_X, 1, Pegboard_X - 1, 2)
    #for (Grid_Y, 1, Room_Y - (Leg_Y + Table_Y) - 1, 2)
      cylinder {
        <Grid_X, Grid_Y, -3>   // Center of near end
        <Grid_X, Grid_Y,  3>   // Center of far  end
        0.25                   // Radius
        pigment { White }
      }
    #end
  #end
}
