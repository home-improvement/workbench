// POV-Ray tutorial
// Followed by Kevin Cole <ubuntourist@hacdc.org> 2018.12.12
//
// 2.2 Getting Started
// http://www.povray.org/documentation/3.7.0/t2_2.html

#include "colors.inc"    // The include files contain
#include "stones.inc"    // pre-defined scene elements
#include "woods.inc"     // pre-defined scene elements

#declare Min_Wall =  0.145;                  // Minimum wall thickness
#declare Avg_OD   =  1.90 + (Min_Wall * 2);  // Average Outer Diameter
#declare Avg_OR   =  Avg_OD / 2;             // Average Outer Radius
#declare Major    =  1.90;                   // tube diameter
#declare Major    =  Avg_OD;                 // tube diameter
#declare Minor    =  Major / 2;              // tube radius
#declare H        = 2.0 + (5 / 16);          // See Charlotte Pipe, Inc. specs
#declare G        = 1.0;                     // See Charlotte Pipe, Inc. specs

#declare Tube_X =  1.90;
#declare Tube_Y = 32.00;
#declare Tube_Z =  1.90;
#declare Tube_L = 3.00;           // tube length
#declare Tube_D =  1.90;           // tube diameter
#declare Tube_R =  Tube_D / 2;     // tube radius

// Lights, camera, action.

// Light positioning will determine where the shadow falls, as well as
// the length of the shadow.  In this case, the shadow should be cast
// to the left and back of the "origin"
//
light_source {
  <20, 10, -20>
  color White
}

// The camera is being focused on a point one unit below it (2 - 1)
// and five units in front of it (-3 - 2).
//
camera {
  location <0, 5, -7.5>
  look_at  <0, 0,  0>
}

// DEBUG
/*
camera {
  location <0, 10, 0>
  look_at  <0, 0,  0>
}
*/

// If no background is specified, the background defaults to black.
//
background { color Cyan }

// Floor
//
// A texture is composed of a pigment, a degree of "bumpiness" and a
// "finish".  There are defaults for the later two, but color must be
// specified. A pigment can be either a pure color, or a color pattern
// such as marble.
//
#declare Floor = plane { <0, 1, 0>, 0
  texture {
    checker texture { T_Grnt19 }, texture { T_Stone5 } scale 5
  }
}

// Transformations occur AFTER the object is defined, unlike
// OpenSCAD.
//
#declare Elbow = union {
  difference {
    // * Major radius is from "origin" to inside center of tube
    // * Minor radius is from inside center of tube to rim of tube
    //
    // Default position is centered (embedded) in origin. Add minor
    // radius value to Y to lift torus fully above the orign plane.
    //
    torus {
      Major, Avg_OR  // Major, Minor
      sturm
    }
    torus {  // Hollow out
      Major, Avg_OR - Min_Wall  // Major, Minor
      sturm
    }
    box {  // Cut "Rear right quarter"
      < 0,  1 * Avg_OD,  0>
      <(Major + Avg_OR), -1 * Avg_OD, (Major + Avg_OR)>
    }
    box {  // Cut "Front half"
      <-1.0 * (Major + Avg_OR), -1 * Avg_OD, -1 * (Major + Avg_OR)>
      < 1.0 * (Major + Avg_OR),  1 * Avg_OD,  0.01>
    }
  }
  difference {
    cylinder {
      <-1.0 * Major, 0, 0.01>
      <-1.0 * Major, 0, -1 * (H - G)>
      Avg_OR
    }
    cylinder {
      <-1.0 * Major, 0,  10>
      <-1.0 * Major, 0, -10>
      Avg_OR - Min_Wall
    }
  }
  difference {
    cylinder {
      <-0.01,       0, 1.0 * Major>
      <1 * (H - G), 0, 1.0 * Major>
      Avg_OR
    }
    cylinder {
      < 10, 0, 1.0 * Major>
      <-10, 0, 1.0 * Major>
      Avg_OR - Min_Wall
    }
  }
  texture {
    pigment { color White }
  }
}

#declare Tube = difference {
  cylinder {
      <0, Tube_R + Min_Wall,  0>      // Center of near end
      <0, Tube_R + Min_Wall, Tube_L>  // Center of far  end
      Tube_R                          // Radius
  }
  cylinder {
      <0, Tube_R + Min_Wall, -10>     // Center of near end
      <0, Tube_R + Min_Wall,  40>     // Center of far  end
      Tube_R - Min_Wall               // Radius
  }
  pigment { Red }
}

union {
  object {
    Floor
  }
  object {
    Elbow
    translate <0, Minor ,0>
  }
  object {
    Tube
    translate <-1.0 * Major, 0, -Tube_L>
  }
  object {
    Tube
    rotate <0, 90, 0>
    translate <0, 0, Major>
  }
}
