// workbench.pov
//
// Custom simple workbench for lectronics and drafting.
// 360 rotations require workbench.ini
//
// Copyright (c) 2018 Kevin Cole <ubuntourist@hacdc.org> 2018.12.12
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free
// Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.
//
// 2015.02.02 KJC - Added new mandatory assumed_gamma setting
//

// NOTE: OpenSCAD and POV-Ray swap Y and Z
//       OpenSCAD Y is "depth of field" and Z is height
//       POV-Ray  Z is "depth of field" and Y is height
//

#include "colors.inc"      // Basic colors
#include "woods.inc"       // Wood grains
#include "stones.inc"      // Stones, marble, etc.
#include "transforms.inc"  // Non-standard transformation macros
/*
#include "textures.inc"
#include "shapes.inc"
#include "glass.inc"
#include "metals.inc"
*/

global_settings {
  max_trace_level 20
  assumed_gamma 1
}

#declare Room_Y = 7.5 * 12;   // Approximate room height in inches

#declare Table_X = 48.00;     // Width of "nook"
#declare Table_Y =  1.50;
#declare Table_Z = 32.00;     // Small enough to fit through a door

#declare Pegboard_X = 48.00;  // Width of the table
#declare Pegboard_Y = 36.00;  // Unused. Use room height to compute.
#declare Pegboard_Z =  0.25;  // How thick are pegboards, typically?

#declare Leg_X =  1.90;
#declare Leg_Y = 33.00;
#declare Leg_Z =  1.90;
#declare Leg_D =  1.90;          // leg diameter
#declare Leg_R =  Leg_D / 2;     // leg radius

// From the right, above, behind camera
//light_source { < 300, 300, -500> White }
light_source { <-250, 300, -200> White }
light_source { <-10, 0, -1000> Gray50 }

// The camera is being focused on a point one unit below it (2 - 1)
// and five units in front of it (-3 - 2).
//
/*
camera {
  location <Table_X / 2, Room_Y - 12, -2.5 * (Table_Z)>
  look_at  <Table_X / 2, Room_Y / 2,  Table_Z / 2>
}
*/
camera {
  location <Table_X + 20, Room_Y + 25, -2 * (Table_Z)>
  look_at  <Table_X / 2,  Room_Y / 2,  Table_Z / 2>
}

//background { color Gray50 }
background { color <0.75, 0.75, 1> }
//sky_sphere { S_Cloud2 }

#declare Floor = plane { <0, 1, 0>, -1
  texture {
    checker texture { T_Grnt19 }, texture { T_Stone5 } scale 5
  }
}

#declare Wall = plane { <0, 0, 1>, 50
  pigment { PaleGreen }
}

#declare Table = box {
  <0, 0, 0>, <Table_X, Table_Y, Table_Z>
  texture { T_Grnt20 }
}

#declare Leg = cylinder {
      <0,  0,    0>   // Center of near end
      <0, Leg_Y, 0>   // Center of far  end
      Leg_R           // Radius
      open            // Hollow pipe (open at both ends)
      pigment { White }
}

#declare Pegboard = difference {
  box {
      <     0,            0,                      0      >,
      <Pegboard_X, Room_Y - (Leg_Y + Table_Y), Pegboard_Z>
      texture { T_Wood4 }  // T_Wood12 T_Wood22
    }
  #for (Grid_X, 1, Pegboard_X - 1, 2)
    #for (Grid_Y, 1, Room_Y - (Leg_Y + Table_Y) - 1, 2)
      cylinder {
        <Grid_X, Grid_Y, -3>   // Center of near end
        <Grid_X, Grid_Y,  3>   // Center of far  end
        0.25                   // Radius
        pigment { White }
      }
    #end
  #end
}

union {
  object {
    Floor
  }
  object {
    Wall
  }
  object {
    Table
    translate <0, Leg_Y, 0>
  }
  object {
    Leg  // Front Left
    translate <Leg_R, 0, Leg_R>
  }
  object {
    Leg  // Front Right
    translate <Leg_R + (Table_X - Leg_X), 0, Leg_R>
  }
  object {
    Leg  // Rear Left
    translate <Leg_R, 0, Leg_R + (Table_Z - Leg_Z)>
  }
  object {
    Leg  // Front Left
    translate <Leg_R + (Table_X - Leg_X), 0, Leg_R + (Table_Z - Leg_Z)>
  }
  object {
    Pegboard
    translate <0, Leg_Y + Table_Y, Table_Z - Pegboard_Z>
  }
}

// Animate!

/*

#declare Angle = 0;
#switch (clock)
  #range (0, 44)
    #declare Angle = clock;
  #break
  #range (45, 134)
    #declare Angle = 45 - (clock - 45);
  #break
  #range (135, 179)
    #declare Angle = (clock - 135) - 45;
  #break
#end
Rotate_Around_Trans(<Angle, 0, 0>, <0, 25.5, 0>)
}
*/
