// POV-Ray tutorial
// Followed by Kevin Cole <ubuntourist@hacdc.org> 2018.12.12
//
// 2.2 Getting Started
// http://www.povray.org/documentation/3.7.0/t2_2.html

#include "colors.inc"    // The include files contain
#include "stones.inc"    // pre-defined scene elements
#include "woods.inc"     // pre-defined scene elements

#declare Leg_X =  4.00;       // (Change to 1.90)
#declare Leg_Y = 33.00;
#declare Leg_Z =  4.00;       // (Change to 1.90)
#declare Leg_D =  4.00;       // leg diameter (Change to 1.90)
#declare Leg_R =  Leg_D / 2;  // leg radius
#declare Ring  =  0.25;

// Lights, camera, action.

// Light positioning will determine where the shadow falls, as well as
// the length of the shadow.  In this case, the shadow should be cast
// to the left and back of the "origin"
//
light_source {
  <20, 10, -20>
  color White
}

// The camera is being focused on a point one unit below it (2 - 1)
// and five units in front of it (-3 - 2).
//
camera {
  location <0, 10, -15>
  look_at  <0, 0,  0>
}

// If no background is specified, the background defaults to black.
//
background { color Cyan }

// Floor                                                                                                                             
//                                                                                                                                   
// A texture is composed of a pigment, a degree of "bumpiness" and a
// "finish".  There are defaults for the later two, but color must be
// specified. A pigment can be either a pure color, or a color pattern
// such as marble.
//
plane { <0, 1, 0>, -1
  texture {
    checker texture {T_Wood1}, texture {T_Wood2}
  }
}

#declare Lip = cylinder {
  <Leg_D, Leg_R + Ring, -0.50>   // Center of near end
  <Leg_D, Leg_R + Ring,  0.50>   // Center of far  end
  Leg_R + Ring                   // Radius
//open
  pigment { color Green }
}


// Transformations occur AFTER the object is defined, unlike
// OpenSCAD.
//
difference {
  union {
    // * Major radius is from "origin" to inside center of tube
    // * Minor radius is from inside center of tube to rim of tube
    //
    // Default position is centered (embedded) in origin. Add minor
    // radius value to Y to lift torus fully above the orign plane.
    //
    torus {
      Leg_D, Leg_R    // major, minor
      sturm
      texture {
        pigment { color White }
      }
      translate <0, Leg_R + Ring, 0>  // Radius of cylinders below
    }
    cylinder {
      <Leg_D, Leg_R + Ring, -0.50>   // Center of near end
      <Leg_D, Leg_R + Ring,  0.50>   // Center of far  end
      Leg_R + Ring                   // Radius
//    open
      pigment { color Green }
    }
    cylinder {
      <-4, 2.25, -0.50>   // Center of near end
      <-4, 2.25,  0.50>   // Center of far  end
      2.25                // Radius
//    open
      pigment { color Green }
    }
    cylinder {
      <-0.50, 2.25, 4>   // Center of near end
      < 0.50, 2.25, 4>   // Center of far  end
      2.25               // Radius
//    open
      pigment { color Green }
    }
  }
  union {
    torus {
      4, 1.75    // major, minor
      sturm
      texture {
        pigment { color White }
      }
      translate <0, 2.25, 0>
    }
    // A cube's location and size are specified by two opposite
    // corners: <left, bottom, front> to <right, top, back>.
    //
    box {
      <-10, -10, -0.51>
      < 10,  10,  -10> 
      texture {
        pigment { color White }
      }
    }
  }
}
